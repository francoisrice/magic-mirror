from django.urls import path
from .views import (
    profile_settings,
    signup,
    UserDeleteView,
)

urlpatterns = [
    path('profile_settings/', profile_settings, name='profile_settings'),
    path('signup/', signup, name='signup'),
    path('user/<int:pk>/delete/',
         UserDeleteView.as_view(), name='user-delete'),
]
