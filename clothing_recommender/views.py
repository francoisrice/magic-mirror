from django.shortcuts import render
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib.auth.models import User
import requests # Make HTTP requests
import os # Read secret keys from environment variables

from django.views.generic import (
    DetailView,
)


class UserHome(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = User
    template_name = 'clothing_recommender/homepage.html'

    def weather_lookup(self, temperature):
        suggestions = {
            "80":"You should wear short sleeves and pants.",
            "66":"You should wear pants and long sleeves.",
            "55":"You should wear long sleeves and a jacket.",
            "40":"Make sure you wear a heavy coat.",
            "30":"Make sure you wear your winter coat, hat, and gloves.",
            "19":"Make sure to wear double layers, of sleeves and pants, and a scarf.",
        }
        
        numbers = [int(num) for num in suggestions.keys()]
        numbers.append(int(temperature))
        numbers.sort(reverse=True)
        a = numbers.index(temperature)-1

        if a >= 0:
            return suggestions[str(numbers[a])]
        else:
            return suggestions["80"]

    def get_context_data(self, **kwargs):
        user = self.get_object()
        context = super(UserHome, self).get_context_data(**kwargs)

        location = str(user.profile.location).strip().split(',')[0] # Remove any whitespace & take everything up to a comma as the location
        location = location.strip().replace(' ','%20') # Change any spaces to unicode

        # Call the geocoding API
        url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/{}.json?country=US&types=place&access_token={}'.format(location,os.environ["GEOCODING_KEY"])
        response = requests.get(url)

        # Take the central GPS coordinates of the first result in the response, returning longitude and latitude, respectively
        gpsCoords = response.json()['features'][0]['center']


        # Call the weather API
        url = 'https://api.darksky.net/forecast/{}/{},{}'.format(
            os.environ["WEATHER_KEY"], str(gpsCoords[1]), str(gpsCoords[0]))

        response = requests.get(url)
        temperature = round(response.json()['currently']['temperature'])

        context['temperature'] = temperature
        
        # Lookup table to suggest clothing to where
        context['suggestion'] = self.weather_lookup(temperature)
        return context

    def test_func(self):
        user = self.get_object()
        if self.request.user == user:
            return True
        return False

def userHome(request):
    return render(request, 'users')


# class UserPostListView(ListView):
#     model = User
#     template_name = 'blog/user_posts.html'

#     def get_queryset(self):
#         return User.location
#         user = get_object_or_404(User, username=self.kwargs.get('username'))
#         return Post.objects.filter(author=user).order_by('-date_posted')
