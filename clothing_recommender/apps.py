from django.apps import AppConfig


class ClothingRecommenderConfig(AppConfig):
    name = 'clothing_recommender'
