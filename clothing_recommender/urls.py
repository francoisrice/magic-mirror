from django.urls import path
from .views import (
    UserHome
 )

urlpatterns = [
    path('user/<int:pk>/homepage/', UserHome.as_view(), name='user-home'),
]
