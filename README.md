## Magic Mirror
### Web application suggesting clothing style based on weather data

#### Current Dependencies:
 pip install 
  - django (web framework)
  - django-crispy-forms (forms styling)
  - Pillow (displaying images)
  - requests (HTTP requests)

#### Start the server:
 - Create a virtualenv with the dependencies
   - $ cd ~
   - python3 -m venv magicmirror
   - $ pip install ...
 - Activate the virtualenv
   - $ source ...
 - Start the server 
  - python3 manage.py runserver

#### Deployment:
 - Magic Mirror is currently deployed on Heroku at https://magic-mirror-wardrober.herokuapp.com/

#### Things to Note:
 - Current configuration only works for United States locations to improve accuracy in the short-term. The current implementation uses the city or town specified as the user's location to calculate local weather. This will cause issues if there are multiple locations with the same name (ex. Troy, NY & Troy, MC). FIX: the state can be search prior to the town, taking the bounding box of the state as a parameter for the town HTTP call. The user's country can additionally be specified and used in the primary call to give international locations. 

#### Current Objectives:
 - Login with email address or username
 - Configure clothing suggestions for different genders/clothing preferences
 - Change Login redirect to user's homepage on signin
   - Need to pass in `user.id` or change homepage to not need it
 - Add unit tests for:
   - Creating a new user
   - Logging in with created User
   - Deleting Created User
   - Accessing user "foo"'s new content as user "bar"
 - Calendar web app
 - Mobile app connected to same database (Webview)
   
#### Future Features:
 - Implement "Forgot Password"
 - Implement Logging; with Heroku Deployment, logs are expected to be sent to STDOUT

#### Necessaries before public launch
 - Remove images from profiles
 - Make background less bright and header brighter
 - Add images for clothing suggestions
 - Add title favicon
 - "Forgot Password" and change password on `profile_settings`

#### Things to Explore:
 - Implement other databases (MongoDB, ElasticStack)
 - Procedurally generate profile pictures
 - Resize images keeping aspect ratio
 - Change where Django looks for `user_confirm_delete.html` from `auth`
    to `users`
 - Have users change password in settings 
    - Use PasswordResetView and PasswordResetDoneView
 - Make pages mobile-first/responsive
 - Send an alert when account has deleted successfully
